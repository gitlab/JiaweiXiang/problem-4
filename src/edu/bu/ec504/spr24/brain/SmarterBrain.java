package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;
import edu.bu.ec504.spr24.sameGameTris.CircleColor;

/**
 * A smarter brain, for you to produce.
 */
public class SmarterBrain extends Brain {
    public SmarterBrain() {
        super();
    }

    private Board currBoard;

    @Override
    public String myName() {
        return "David's algo";
    }

    @Override
    Pos nextMove() {
//      JOptionPane.showMessageDialog(null, "Please insert brain here!");
        // Initialize and set up internal board
        currBoard = new Board();
        for(int xx = 0; xx < myGUI.boardWidth(); xx++) {
            for(int yy = 0; yy < myGUI.boardHeight(); yy++) {
                currBoard.modify(xx, yy, myGUI.colorAt(xx,myGUI.boardHeight() - yy - 1));
            }
        }
        return chooseMove();
    }

    /**
     * Choose the next move to make
     * an extension of the Lazy brain
     *
     * @return a Board coordinates, where is the click
     */
    private Board.Pos chooseMove() {
        int max = 0;
        Board.Pos bestPos = new Board.Pos(0,0);
        Board currStateCopy = new Board(currBoard);
        boolean top = false;
        int topCol = 0;

        for(int xx = 0; xx < currBoard.columns(); xx++) {
            if(currStateCopy.getAt(xx, 7) != CircleColor.NONE) {
                top = true;
                topCol = xx;
                break;
            }

        }

        if(top) {
            for(int xx = 0; xx < currBoard.rows(topCol); xx++) {
                Board test = new Board(currStateCopy);

                currStateCopy.clickNodeHelper(topCol, xx, test.getAt(xx, topCol));

                int cnt = test.clickNode(topCol, xx);
                if(cnt > max) {
                    max = cnt;
                    bestPos = new Board.Pos(topCol, xx);
                }
            }
        } else {
            for(int xx = 0; xx < currBoard.columns(); xx++) {
                for(int yy = 0; yy < currBoard.rows(xx); yy++) {
                    if(currStateCopy.getAt(xx, yy) != CircleColor.NONE) {
                        Board test = new Board(currStateCopy);

                        currStateCopy.clickNodeHelper(xx, yy, test.getAt(xx, yy));

                        int cnt = test.clickNode(xx, yy);
                        if(cnt > max) {
                            max = cnt;
                            bestPos = new Board.Pos(xx, yy);
                        }
                    }
                }
            }
        }

        bestPos = new Board.Pos(bestPos.xx, myGUI.boardHeight() -1 - bestPos.yy);
        return bestPos;
    }
}
